var should = require('should');

var ioclient = require('socket.io-client');
var app = require('../app');
var io = app.get('io');

var socketURL = 'http://localhost:9000';

var options = {
  	transports: ['websocket'],
  	'force new connection': true
};

var token = 'WoodV6s8mK4m7PC79tzirVrTlp-FB1s7',
	token2 = 'R9fbmQsttnsyHf_gBMRNPfAPknbTkuG7';
var id = 15, id2 = 16;

function connectClient(token, cb) {
	var client = ioclient.connect(socketURL, options);

	client.on('connect', function() {
		client.emit('authentication', {token: token});
		client.on('authenticated', function() {
			cb(client);
		})
	})
}

describe('Chat server', function() {
	describe('Authentication', function() {
		
		/* login */
		it('Should disconnect without token', function(done) {
			var client = ioclient.connect(socketURL, options);

			client.on('disconnect', function() {
				done();
			})
		})

		/* mobile app */
		it('Should authenticate with token', function(done) {

			connectClient(token, function(client) {
				done();
				client.disconnect();
			})
		})

		/* site - admin */
		it('Should authenticate from admin\'s page', function(done) {
			var client = ioclient.connect(socketURL, options);
			client.on('connect', function() {
				client.emit('authentication', {admin: true});
				client.on('authenticated', function() {
					done();
					client.disconnect();
				})
			})
		})

		it('Should create four new rooms', function(done) {

			connectClient(token, function(client) {
				connectClient(token2, function(second) {
					Object.keys(io.sockets.adapter.rooms).length.should.equal(4);
					done();
					client.disconnect();
					second.disconnect();
				})
			})
		})

	})

	describe('Messaging', function() {
		/* admin1 and client1 in one room */

		var client1, client2, admin1, admin2;
		var func1, func2, aFunc1, aFunc2;
		var aName1 = 'admin';
		afterEach(function() {
			client1.removeListener('message', func1);
			client2.removeListener('message', func2);
			admin1.removeListener('message', aFunc1);
			admin2.removeListener('message', aFunc2);
		})
		before(function(done) {
			Promise.all([
				new Promise(function(resolve, reject) {
					connectClient(token, function(client) {
						client1 = client;
						resolve();
					})
				}),
				new Promise(function(resolve, reject) {
					connectClient(token2, function(client) {
						client2 = client;
						resolve();
					})
				}),
				new Promise(function(resolve) {
					admin1 = ioclient.connect(socketURL, options);
					admin1.on('connect', function() {
						admin1.emit('authentication', {admin: true, dialog: id});
						admin1.on('authenticated', function() {
							resolve();
						})
					})
				}),
				new Promise(function(resolve) {
					admin2 = ioclient.connect(socketURL, options);
					admin2.on('connect', function() {
						admin2.emit('authentication', {admin: true});
						admin2.on('authenticated', function() {
							resolve();
						})
					})
				})
			]).then(function(resolve) {
				done();
			})
		})

		it('Client 2 should not get message from client 1', function(done) {
			var gotMessages = 0;
			func2 = function(data) {
				gotMessages++;
			}
			client2.on('message', func2);
			client1.emit('message', {message: 'client 1'});
			setTimeout(function() {
				gotMessages.should.equal(0);
				done();
			}, 200);
		})

		it('Admin 1 should get message from client1', function(done) {
			var mess = 'client1'
			aFunc1 = function(data) {
				data.message.should.equal(mess);
				done();
			}
			admin1.on('message', aFunc1);
			client1.emit('message', {message: mess});
		})

		it('Admin 2 should not get message from client1', function(done) {
			var adminGot = 0;

			aFunc2 = function(data) {
				adminGot++;
			}
			admin2.on('message', aFunc2);
			client1.emit('message', {message: 'mess'});
			setTimeout(function() {
				adminGot.should.equal(0);
				done();
			}, 100);
		})

		it('Client 1 should receive admin\'s name when he types', function(done) {
			client1.on('typing', function(data) {
				data.name.should.equal(aName1);
				done();
			});

			admin1.emit('typing');
		})

		it('Should send alert to lobby when there is no admin in user\'s dialogue', function(done) {
			admin2.on('new message', function(data) {
				data.id.should.equal(id2);
				done();
			})

			client2.emit('message', {message: 'anything'});
		})

		it('Client 1 should not get technical message from admin', function(done) {
			var gotMessages = 0;
			func1 = function(data) {
				gotMessages++;
			}

			client1.on('message', func1);

			admin1.emit('message', {message: 'it\'s not for all', tech: true});
			setTimeout(function() {
				gotMessages.should.equal(0);
				done();
			}, 200);
		})
	})

})
//				console.log(app.get('io').sockets.adapter.rooms)