var should = require('should');
var message = require('../models/message');

describe('Models', function() {
	describe('Messages', function() {
		it('Should save message from user', function() {
			var values = {
				user_id: 15,
				message: 'Test message'
			}
			message.write(values, function(err, row) {
				err.should.be.exactly(null);
				done();
			})
		})

		it('Should save message from admin', function() {
			var values = {
				admin_id: 4,
				file: '/uploads/image.jpg',
				tech: true
			}
			message.write(values, function(err, row) {
				err.should.be.exactly(null);
				done();
			})
		})
	})
})