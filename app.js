var express = require('express');
var path = require('path');
var fs = require('fs');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var socket = require('./socket');
var multiparty = require('multiparty');
var app = express();
var user = require('./models/user');
var message = require('./models/message');

const LIMIT = 10 * 1024 * 1024;

var env = process.env.NODE_ENV || 'development';
var domain = require('./config/domain.json')[env];

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json({limit:'100000000'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res, next) {
	user.getFirst(function(err, result) {
		message.get({
			id: result.id,
			tech: false,
//			limit: 10
		}, function(err, rows) {
			if (err) return next(err);
			res.render('client', {rows: rows, token: result.auth_key});
		})
		// body...
	})
})

app.get('/admin', function(req, res, next) {
	user.getAll(function(err, result) {
		if (err) return next(err);
		res.render('support', {users: result});
	})
});

app.get('/admin/dialog/:id', function(req, res, next) {
	message.get({
		id: req.params.id,
		tech: true
	}, function(err, rows) {
		if (err) return next(err);
		res.render('supp-dialog', {currD: req.params.id, rows: rows});
	})
});

app.get('/history/', function(req, res, next) {
	user.history(req.query.token, req.query.offset, req.query.limit, function(err, messages) {
		if (err) {
			console.log(err);
			return res.json({
				error: 'Server error'
			})
		}
		res.json(messages);
	})
})

app.post('/upload', function(req, res, next) {
	var form = new multiparty.Form({maxFieldsSize: LIMIT});
	var newFileName;
	
	form.on('file', function(name, file) {
		if (newFileName) return;
        newFileName = '/uploads/' + path.basename(file.path);
        fs.rename(
            file.path,
            __dirname + '/public' + newFileName,
            function(err){
                if (err){
                	console.log(err);	
                	return res.json({
                		error: 'Internal error'
                	});
                }
				res.json({
					file: domain + newFileName
				})  
            }
        )
	})

	form.on('error', function(err) {
		console.log(err);
		res.json({
			error: err
		});
	})

	form.parse(req);
})

var server = app.listen(9000, function () {
	console.log('App listening on port 9000!');
});

var io = socket(server);
app.set('io', io);

module.exports = app;

