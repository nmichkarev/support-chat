var socketio = require('socket.io');
var sockauth = require('socketio-auth');
var request = require('request');
var user = require('../models/user');
var admindb = require('../models/admin');
var message = require('../models/message');

const PUSH_ADDRESS = '';

var dialogs = {};
var greeted = {};
var io;

const REPLY_TEXT = "Ваше сообщение получено. Специалист ответит вам в ближайшее время. Спасибо.";

module.exports = function (server) {
	io = socketio.listen(server, { pingTimeout: 4000, pingInterval: 4000 });

	sockauth(io, {
		authenticate: function (socket, data, callback) {
			if (data.admin) {
				enterAdmin(socket, data.dialog);
				callback(null, true);
			} else {
				user.getByToken(data.token, function(err, usr) {
					if (err) return callback(err);
					if (!usr) return callback(new Error('User not found'));

					socket['db_id'] = usr.id;
					enterUser(socket, data.token);
					joinDialog(socket, usr.id);
					callback(null, true);
				})
			}
		},
		timeout: 1000
	})

	io.sockets.on('connection', function(socket) {
		socket.emit('join', 'hello there')
	})
	return io;
}

function enterUser(socket, token) {
	commonMessagesHandler(socket);
	socket.token = token;
}

function enterAdmin(socket, dialog) {
	if (dialog) {
		user.getById(dialog, function(err, usr) {
			if (err || !usr) {
				return socket.disconnect();
			}
			socket.room_id = usr.id;
			joinDialog(socket, usr.id);
		})
	} else {
		joinDialog(socket, 'lobby');
	}
	admindb.getById(4, function(err, result) {
		socket.admin = result.name || 'admin';
		socket.avatar = result.avatar;
		socket.db_id = 4;
	})
	commonMessagesHandler(socket);
}

/* Вход в диалог. Имя диалога - id обратившегося пользователя */
function joinDialog(socket, id) {
	dialogs[socket.id] = id + '';
	socket.join(id);
	socket.on('disconnect', function() {
		delete dialogs[socket.id];
	})
}

/* Общие обработчики для админа и юзера */
function commonMessagesHandler(socket) {

	socket.on('message', function(data) {
		if (dialogs[socket.id] == 'lobby') return;
		if (socket.avatar) data.avatar = socket.avatar;
		if (!data.text && !data.image) return;

		sendToRoom(socket, data);
		
		if (socket.admin) {
			sendPUSH(socket);
		} else {
			usersMessage(socket, data);
		}

		console.log('MESSAGE', data);
	});

	socket.on('error', function(err) {
		console.log('Socket error');
		console.error(err);
	})

	socket.on('typing', function() {
		socket.broadcast.to(dialogs[socket.id]).emit('typing', {name: socket.admin});
	})
}

function sendToRoom(socket, data) {

// Если техническое сообщение, то отсылаем только админам, если они есть в комнате
	if (socket.admin && data.tech) {
		var room = io.sockets.adapter.rooms[dialogs[socket.id]].sockets;
		for (var i in room) {
			var sock = io.sockets.connected[i];
			if(sock.admin && sock !== socket) sock.emit('message', data);
		}
	} else {
		socket.broadcast.to(dialogs[socket.id]).emit('message', data);
	}

	messageToDB(socket, data);
}

/* Отсылаем в лобби если в комнате нет админа */
function usersMessage(socket, data) {
	var room = io.sockets.adapter.rooms[socket.db_id].sockets;
	var have = false;
	for (var i in room) {
		var sock = io.sockets.connected[i];
		if(sock.admin) {
			have = true;
			break;
		}
	}
	if (!have) {
		if (!(socket.db_id in greeted)) {
			socket.emit('message', {text: REPLY_TEXT});
			greeted[socket.db_id] = true;
		}
		socket.broadcast.to('lobby').emit('new message', {id: socket.db_id})
	}
}

// Сохранение сообщения в базу данных
function messageToDB(socket, data, cb) {
	var values = {};

	if (socket.admin) {
		values.admin_id = socket.db_id;
		values.tech = data.tech || 0;
		values.user_id = socket.room_id;
	} else {
		values.user_id = socket.db_id;
	}

	if (data.text) values.message = data.text;
	if (data.image) values.file = data.image;

	message.write(values)
}

function sendPUSH(socket) {
	var room = io.sockets.adapter.rooms[dialogs[socket.id]].sockets;
/*	var have = false; // есть ли юзер в чате

	for (var i in room) {
		var sock = io.sockets.connected[i];
		if(!sock.admin && sock.token) {
			have = true;
		}
	}

	if (!have) {*/
		user.getById(dialogs[socket.id], function(err, user) {
			if (!err && user) {
				request.post({
					url: PUSH_ADDRESS,
					form: {
						token: user.auth_key
					}
				}, function (error, response, body) {
					console.log('Request sent');
					console.log('URL: ', PUSH_ADDRESS);
					console.log('token: ', user.auth_key);
					if (error) console.log(error);
					console.log('body: ', body);
					if (!error && response && response.statusCode == 200) {
						//console.log(body)
					} else {
						if (response) {
							console.log('response status', response.statusCode);
						} else {
							console.error('Unexpected error');
						}
					}
				})	
			}
		})
//	}
}