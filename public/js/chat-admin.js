(function() {
	var history = document.getElementById('history');
	var typing = document.getElementById('typing');
	var out, clock;
	localStorage.debug='*';
	window.onload = function() {
		var socket = io.connect('', {
		   "transports": ['websocket']
		});

		socket
			.on('connect', function() {
				console.log('connect')
				socket.emit('authentication', {admin: true, dialog: currentDialog});
			})
			socket.on('authenticated', function() {
				console.log('authenticated');
			})
			.on('disconnect', function() {
				console.log('disconnect')
			})
			.on('error', function() {
				console.log('error')
			})
			.on('message', function(data) {
				newMessage(data);
			})
			.on('typing', function(data) {
				typing.innerHTML = data.name || 'собеседник' + ' печатает...';
				clearTimeout(clock);
				clock = setTimeout(function() {
					typing.innerHTML = '';
				}, 1000);
			})
			.on('new message', function(data) {
				console.log(data);
			})

		$('#send-message').on('click', function(evt) {
			var text = document.getElementById('message').value;
			var image = document.getElementById('chatimage').dataset.ref;
			if (!text && !image) {
				return $('.entering').addClass('error');
			}
			var data = {
				text: text,
				image: image
			};

			if ($('#tech')[0].checked) {
				data.tech = 1;
			}

			socket.emit('message', data);
			newMessage(data, true);
			clearFields();
		})

		$('#message').on('input', function(evt) {
			socket.emit('typing');
		})

		scrollHistory();
	}

	function clearFields() {
		document.getElementById('message').value = '';
		document.getElementById('message').innerHTML = '';
		$('.mess-images')[0].innerHTML = '';
		delete $('[name=chatimage]')[0].dataset.ref;
		$('.entering').removeClass('error');
	}

	function newMessage(data, you) {
		var el = document.createElement('div');

		var time, timeEl;

		time = data.created_at ? new Date(data.created_at * 1000) : new Date();
		timeEl = document.createElement('DIV');
		timeEl.className = 'message-time';
		timeEl.innerHTML = time.toLocaleDateString() + ' ' + time.toLocaleTimeString();

		el.appendChild(timeEl);

		el.className = 'message-row' + (you ? ' you' : '');
		if (data.image) {
			var img = document.createElement('img');
			img.setAttribute('src', data.image);
			img.onclick = function() {
				window.open(this.src);
			}
			el.appendChild(img);
		}
		if (data.text) {
			var txt = document.createElement('p');
			txt.innerHTML = data.text;
			el.appendChild(txt);
		}
		history.appendChild(el);
		
		scrollHistory();
	}

	function scrollHistory() {
		var scrollHeight = Math.max(history.scrollHeight, history.clientHeight);
   		history.scrollTop = scrollHeight - history.clientHeight;
	}

})()