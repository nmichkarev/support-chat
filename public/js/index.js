document.forms.upload.onsubmit = function() {
    var self = this;
    var file = this.elements.chatimage.files[0];
    if (file) {
        upload(file, function(ref) {
            try{
                var fname = JSON.parse(ref);
                fname = fname.file;
                appendImage(fname);
                self.elements.chatimage.value = '';
            } catch(e){
                console.error(e);
                
            }
        });
    }
    return false;
}

function log(html) {
    document.getElementById('log').innerHTML = html;
}
function upload(file, cb) {

    var xhr = new XMLHttpRequest();

//    xhr.setRequestHeader('Content-Type', 'multipart/form-data');

    // обработчики можно объединить в один,
    // если status == 200, то это успех, иначе ошибка
    xhr.onload = xhr.onerror = function() {
        if (this.status == 200) {
            log("success");
        } else {
            log("error " + this.status);
        }
    };

    xhr.onloadend = function() {
        cb(this.responseText)
    }

    // обработчик для закачки
    xhr.upload.onprogress = function(event) {
        log(event.loaded + ' / ' + event.total);
    }

    xhr.open("POST", "/upload", true);
    var formData = new FormData();
    formData.append("myfile", file);
    xhr.send(formData);
//    xhr.send(file);

}

function appendImage(ref) {
    $('.mess-images')[0].innerHTML = '<img src="' + ref + '">';
    $('[name=chatimage]')[0].dataset.ref = ref;
}
