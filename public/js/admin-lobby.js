var socket = io.connect('', {
   "transports": ['websocket']
});

socket
	.on('connect', function() {
		console.log('connect')
		socket.emit('authentication', {admin: true});
		socket.on('authenticated', function() {
			console.log('authenticated');
		})
	})
	.on('disconnect', function() {
		console.log('disconnect')
	})
	.on('error', function() {
		console.log('error')
	})
	.on('new message', function(data) {
		incMess(data);

	})

function incMess(data) {
	var el = $('[data-dialog=' + data.id + ']')[0];
	var count = parseInt(el.innerHTML);
	count = isNaN(count) ? 1 : ++count;
	el.innerHTML = count;
}