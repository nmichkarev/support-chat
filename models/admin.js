var db = require('./');
var table = 'admin';

exports.getFirst = function(callback) {
	db
		.limit(1)
		.get(table, function(error, result, fields) {
			if (error) return callback(error);
			callback(null, result[0])
		})
}

exports.getById = function(id, callback) {
	db
		.where({id: id})
		.get(table, function(error, result, fields) {
			if (error) return callback(error);
			callback(null, result[0])
		})
}