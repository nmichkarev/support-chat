var db = require('./');
var message = require('./message.js');

var table = 'user';

exports.getByToken = function(token, callback) {
	db
		.where({
			auth_key: token
		})
		.get(table, function(error, result, fields) {
			if (error) console.log(error);
			if (error) return callback(error);
			callback(null, result[0]);
		})
}

exports.getById = function(id, callback) {
	db
		.where({
			id: id
		})
		.get(table, function(error, result, fields) {
			if (error) console.log(error);
			if (error) return callback(error);
			callback(null, result[0]);
		})
}

exports.getAll = function(callback) {
	db
		.get(table, function(error, result, fields) {
			if (error) return callback(error);
			callback(null, result, fields);
		})
}

exports.getFirst = function(callback) {
	db
		.limit(1)
		.get(table, function(error, result, fields) {
			if (error) return callback(error);
			callback(null, result[0])
		})
}

exports.history = function(token, offset, limit, callback) {
	exports.getByToken(token, function(err, user) {
		if (err) return callback(err);
		if (!user) return callback(new Error(404));
		message.get({
			id: user.id,
			limit: limit,
			offset: offset,
			revert: true
		}, function(err, messages) {
			if (err) return callback(err);
			callback(null, messages);
		})
	})
}