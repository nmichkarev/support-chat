var env = process.env.NODE_ENV || 'development';

var mysql = require('mysql-activerecord');
var config = require('../config/config.json')[env];

var db = mysql.Adapter(config);

module.exports = db;
// module.exports = db_pool;