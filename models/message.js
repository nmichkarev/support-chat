var db = require('./');
var table = 'message';
var userTable = 'user';

/*exports.get = function(opts, callback) {
	var where = "WHERE u.id=" + opts.id + " " + (!opts.tech ? "AND is_technical=0 " : "");
	var limit = opts.limit ? " LIMIT " + opts.limit : "";
	var sql = "SELECT m.message, m.admin_id, m.user_id, m.file, m.created_at, m.is_technical, a.username as admin_name, u.id as user_id FROM " +
			table + " m LEFT JOIN user u ON m.user_id=u.id " +
			"LEFT JOIN admin a ON m.admin_id=a.id " +
			where + 
			limit;

	db
		.query(sql, function (err, result, fields) {
			if (err) return callback(err);
			callback(null, result);
		})
}*/

exports.get = function(opts, callback) {
	var where = " WHERE user_id=" + opts.id + " " + (!opts.tech ? "AND is_technical=0 " : "");
	var limit = opts.limit 
		? " LIMIT " + (
			opts.offset 
			? opts.offset + ", " 
			: "") +
			opts.limit
		: "";
	var order = opts.revert ? " ORDER BY created_at DESC" : "";
	var sql = "SELECT message, file, created_at, is_technical, user_id, admin_id FROM " +
			table +
			where + 
			order +
			limit;

	db
		.query(sql, function (err, result, fields) {
			if (err) return callback(err);
			callback(null, result);
		})
}

exports.write = function(data, callback) {
	if (!data.user_id && !data.admin_id) {
		return callback(new Error('No sender'));
	}

	var values = {
		created_at: (Math.round((new Date).valueOf() / 1000)),
		is_technical: (data.tech ? 1 : 0)
	};
	['admin_id', 'user_id', 'file', 'message'].forEach(function(field) {
		if (field in data) {
			values[field] = data[field]
		}
	})

	db
		.insert(table, values, function(err, message) {
			if (err) return callback(err);
			callback && callback(null, message);
		})
}


